;; -*- lexical-binding: t; -*-

(when (require 'gcmh nil t)
  (gcmh-mode)
  (setcar (cdr (assq 'gcmh-mode minor-mode-alist)) nil))

(require 'subr-x)

(defun /source-file (filename)
  (interactive "fSource file: ")
  (when (file-exists-p filename)
    (with-temp-buffer
      (call-process-shell-command
       (format "diff -u <(true; export) <(source %s; export)" filename) nil t)
      (goto-char (point-min))
      (let ((envvar-re "declare -x \\([^=]+\\)=\\(.*\\)$"))
        (while (re-search-forward (concat "^-" envvar-re) nil t)
          (setenv (match-string 1) nil))
        (goto-char (point-min))
        (while (re-search-forward (concat "^+" envvar-re) nil t)
          (setenv (match-string 1) (read (match-string 2))))))))

(defun /add-to-path (new-path &optional path)
  (let ((path (or path "PATH"))
        (new-path* (concat (expand-file-name new-path) ":")))
    (unless (string-match-p new-path* (getenv path))
      (setenv path (concat new-path* (getenv path))))))

(defun /has-already-started-p (process)
  (thread-last
    process (concat "pgrep --full ") call-process-shell-command (equal 0)))

(defconst /nb-proc (number-to-string (1- (num-processors))))

(defun /setup-kinesis ()
  (let ((call
         (lambda (kinesis)
           (call-process-shell-command
            (format
             "setxkbmap -device $(xinput list --id-only keyboard:'%s') fr bepo"
             kinesis))))
        key)
    (when (equal (funcall call "Kinesis Advantage2 Keyboard") 0)
      (setq key "Escape"))
    (when (equal (funcall call "Kinesis Kinesis Adv360") 0)
      (setq key "underscore"))
    (when key
      (call-process-shell-command
       (string-join
        (list
         "xmodmap"
         "-e 'clear mod4'"
         "-e 'clear lock'"
         "-e 'keycode 94 = underscore'"
         "-e " (concat "'keycode 66 = " key "'")
         "-e 'keycode 134 = ISO_Level3_Shift'")
        " "))
      (setopt
       exwm-input-move-event (kbd "M-<down-mouse-1>")
       exwm-input-resize-event (kbd "M-<down-mouse-3>")))))

(defvar /setup-kinesis-timer nil)
(defun /defer-setup-kinesis ()
  (when /setup-kinesis-timer (cancel-timer /setup-kinesis-timer))
  (setq /setup-kinesis-timer (run-with-timer 5 nil #'/setup-kinesis)))

(defun /move-beginning-of-line ()
  (interactive)
  (let ((orig-point (point)))
    (back-to-indentation)
    (when (equal orig-point (point)) (move-beginning-of-line 1))))
(keymap-global-set
 "<remap> <move-beginning-of-line>" #'/move-beginning-of-line)

(defun /move-buffer-next-window ()
  (interactive)
  (set-window-buffer (next-window) (window-buffer (selected-window)))
  (previous-buffer))
(keymap-global-set "M-o" #'/move-buffer-next-window)

(defun /replace-spaces-before-save-advice (args)
  (list (replace-regexp-in-string "[[:space:]\n]+" " " (car args))))

(defvar-local /complete-history-current nil)
(defun /complete-history-function (collection)
  (lambda (string pred action)
    (if (eq action 'metadata)
        `(metadata (cycle-sort-function . ,#'identity))
      (let ((collection*
             (if (ring-p collection) (ring-elements collection) collection)))
        (complete-with-action action collection* string pred)))))
(defun /complete-history (name history)
  (lambda ()
    (interactive)
    (let ((history* (symbol-value history)))
      (setq-local /complete-history-current history*)
      (thread-last
        (/complete-history-function history*)
        (completing-read (concat name " history: "))
        insert))))
(defun /complete-history-delete (&optional candidate)
  (interactive)
  (let ((candidate*
         (or candidate
             (with-current-buffer (window-buffer (minibuffer-window))
               (car completion-all-sorted-completions)))))
    (with-current-buffer (window-buffer (minibuffer-selected-window))
      (if (ring-p /complete-history-current)
          (while
              (when-let
                  ((idx (ring-member /complete-history-current candidate*)))
                (ring-remove /complete-history-current idx)
                t))
        (delete candidate* /complete-history-current))
      (kill-new candidate*)))
  (with-current-buffer (window-buffer (minibuffer-window))
    (icomplete-forward-completions)
    (completion--flush-all-sorted-completions))
  (set-transient-map
   (let ((xkmap (make-sparse-keymap)))
     (keymap-set xkmap "d" #'/complete-history-delete)
     xkmap)))

(defun /protect-spaces (input)
  (thread-last
    input
    (string-replace "\\ " "⊥")
    (string-replace " " ".*")
    (string-replace "⊥" " ")))
(defun /project-find-regexp (&optional arg)
  (interactive "P")
  (require 'project)
  (require 'grep)
  (require 'vc-git)
  (when-let ((grep-buffer (get-buffer "*grep*"))) (kill-buffer grep-buffer))
  (let* ((files
          (if arg
              (grep-read-files (grep-tag-default))
            (cdr (assoc "all" grep-files-aliases))))
         (files* (mapconcat #'shell-quote-argument (split-string files) " "))
         (dir
          (if arg
              (read-directory-name "In directory: " nil default-directory t)
            (project-root (project-current))))
         (call-grep
          (lambda ()
            (if (minibufferp)
                (let ((input (minibuffer-contents-no-properties)))
                  (when (> (length input) 2)
                    (when (get-buffer "*grep*")
                      (with-current-buffer "*grep*"
                        (let ((comp-proc (get-buffer-process (current-buffer))))
                          (when
                              (and
                               comp-proc
                               (equal (process-status comp-proc) 'run))
                            (condition-case ()
                                (progn
                                  (interrupt-process comp-proc)
                                  (sit-for 1)
                                  (delete-process comp-proc))
                              (error nil))))))
                    (vc-git-grep (/protect-spaces input) files* dir)))
              (exit-minibuffer)))))
    (unwind-protect
        (progn
          (add-hook 'post-command-hook call-grep)
          (let ((input (grep-read-regexp)))
            (when (and (not (string-empty-p input)) (not (get-buffer "*grep*")))
              (let ((current-prefix-arg nil)) (vc-git-grep input files* dir)))
            (when-let ((grep-buffer (get-buffer "*grep*")))
              (pop-to-buffer grep-buffer)
              (rename-buffer (concat (buffer-name) ": " input) t))))
      (remove-hook 'post-command-hook call-grep))))
(keymap-global-set "<remap> <project-find-regexp>" #'/project-find-regexp)

(defun /switch-to-buffer ()
  (interactive)
  (let* ((buffers
          (thread-last
            (buffer-list)
            (delete (current-buffer))
            (mapcar #'buffer-name)
            (seq-filter (lambda (n) (equal (string-trim-left n) n)))))
         (buffer-file-names (seq-keep #'buffer-file-name (buffer-list)))
         (files
          (seq-remove
           (lambda (f) (member (expand-file-name f) buffer-file-names))
           recentf-list))
         (buffers+files (append buffers files))
         (buffer-or-file
          (completing-read
           "Switch to buffer or file: "
           (lambda (string pred action)
             (if (eq action 'metadata)
                 `(metadata (cycle-sort-function . ,#'identity))
               (complete-with-action action buffers+files string pred)))
           nil
           t)))
    (if (get-buffer buffer-or-file)
        (switch-to-buffer buffer-or-file)
      (find-file buffer-or-file))))
(keymap-global-set "<remap> <switch-to-buffer>" #'/switch-to-buffer)

(defun /call-mpv (url)
  (start-process-shell-command
   "mpv" "mpv" (format "mpv --save-position-on-quit %s" url)))
(defun /call-feh (file)
  (start-process-shell-command
   "feh" "feh" (format "feh --edit -F --start-at %s" file)))
(keymap-global-set "<f9>" #'bookmark-bmenu-list)

(let ((ssh-socket "/tmp/ssh-auth-sock")
      (ssh-pid "/tmp/ssh-agent-pid"))
  (if (file-exists-p ssh-socket)
      (progn
        (setenv "SSH_AUTH_SOCK" (expand-file-name ssh-socket))
        (setenv
         "SSH_AGENT_PID"
         (with-temp-buffer (insert-file-contents ssh-pid) (buffer-string))))
    (let ((temp-file (make-temp-file ""))
          (command (format "ssh-agent -a %s" ssh-socket)))
      (call-process-shell-command command nil (list :file temp-file))
      (/source-file temp-file)
      (with-temp-buffer
        (insert (getenv "SSH_AGENT_PID"))
        (write-region nil nil ssh-pid)))))

(setopt
 custom-file (expand-file-name "custom.el" user-emacs-directory)
 custom-safe-themes t
 use-short-answers t
 view-read-only t
 initial-major-mode 'text-mode
 backup-directory-alist
 (thread-last (expand-file-name "backups" user-emacs-directory) (cons ".") list)
 backup-by-copying t
 version-control t
 delete-old-versions t
 scroll-step 1
 async-shell-command-buffer 'new-buffer
 show-paren-context-when-offscreen 'overlay
 mode-line-format (remove '(vc-mode vc-mode) mode-line-format)
 mode-line-compact t
 delete-by-moving-to-trash t
 window-combination-resize t)
(put 'downcase-region 'disabled nil)

(prefer-coding-system 'utf-8)
(write-region "" nil custom-file)
(/add-to-path "~/Documents/programmes/shell")
(add-to-list 'load-path "~/Documents/programmes/lisp/elisp")
(add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono"))
(keymap-global-set "<remap> <kill-buffer>" #'kill-this-buffer)
(when (display-graphic-p)
  (tool-bar-mode -1)
  (scroll-bar-mode -1))
(menu-bar-mode -1)
(delete-selection-mode)
(winner-mode)
(column-number-mode)
(repeat-mode)
(pixel-scroll-precision-mode)
(recentf-mode)
(fido-vertical-mode)

;; TODO: completion-pcm-leading-wildcard
;;       (partial-completion ((completion-pcm-leading-wildcard t)))
;;       (partial-completion ((completion-pcm-leading-wildcard nil)))
(when (require 'orderless nil t)
  (setopt
   completion-styles '(orderless basic)
   completion-category-defaults nil
   completion-category-overrides '((file (styles partial-completion)))
   orderless-component-separator #'orderless-escapable-split-on-space))

(when (require 'expand-region nil t)
  (keymap-global-set "C-=" #'er/expand-region))

(when (require 'multiple-cursors nil t)
  (keymap-global-set "C-S-c C-S-c" #'mc/edit-lines)
  (keymap-global-set "C->" #'mc/mark-next-like-this)
  (keymap-global-set "C-<" #'mc/mark-previous-like-this)
  (keymap-global-set "C-c C-<" #'mc/mark-all-like-this)
  (keymap-unset mc/keymap "<return>")
  (setopt mc/always-run-for-all t))

(when (require 'gruvbox nil t)
  (load-theme 'gruvbox-light-soft t)
  (load-theme 'gruvbox-dark-hard t)
  (defun /switch-gruvbox ()
    (interactive)
    (load-theme (cadr custom-enabled-themes)))
  (keymap-global-set "<f7>" #'/switch-gruvbox))

;; TODO; https://codeberg.org/emacs-weirdware/lemon/pulls/3
(add-to-list 'load-path "~/Documents/programmes/lisp/elisp/lemon")

(when (require 'lemon nil t)
  (require 'lemon-time) (set-face-foreground 'lemon-time-face nil)
  (defclass /lemon-sep (lemon-monitor) ())
  (cl-defmethod lemon-monitor-display ((_ /lemon-sep)) "  ")
  (cl-defmethod lemon-monitor-fetch ((_ /lemon-sep)))
  (let* ((monitors
          '((lemon-time :display-opts '(:format "%a %d %b %H:%M"))
            (lemon-temp
             :device (lemon-temp-coretemp-device)
             :display-opts '(:index "" :sparkline nil))
            (lemon-cpu-linux :display-opts '(:sparkline nil))
            (lemon-memory-linux :display-opts '(:sparkline nil))
            (lemon-battery)
            (lemon-linux-network-rx :display-opts '(:sparkline nil))
            (lemon-linux-network-tx :display-opts '(:sparkline nil))))
         (monitors*
          (seq-reduce
           (lambda (ms m) (append ms (list m #'/lemon-sep))) monitors nil)))
    (setq lemon-monitors (list monitors*)))
  (lemon-mode))

(when (require 'shell-switcher nil t) (shell-switcher-mode))

(when (require 'exwm nil t)
  (keymap-unset exwm-mode-map "C-s")
  (defun /exwm-setup-update-class ()
    (exwm-workspace-rename-buffer exwm-class-name))
  (add-hook 'exwm-update-class-hook #'/exwm-setup-update-class)
  (defun /exwm-setup-init ()
    (call-process-shell-command "setxkbmap fr bepo")
    (/setup-kinesis)
    (start-process-shell-command "xss-lock" nil "xss-lock slock")
    (unless (/has-already-started-p "firefox")
      (start-process-shell-command "firefox" nil "firefox"))
    (advice-remove 'save-buffers-kill-terminal #'exwm--confirm-kill-terminal)
    (let ((interface "org.freedesktop.PolicyKit1.Authority"))
      (dbus-register-signal
       :system nil nil interface "Changed" #'/defer-setup-kinesis))
    (setopt
     exwm-input-global-keys
     (mapcar
      (lambda (keys) (cons (kbd (car keys)) (cadr keys)))
      '(("<end>"
         (lambda ()
           (interactive)
           (if (equal 0 exwm-workspace-current-index)
               (exwm-workspace-switch 1)
             (exwm-workspace-switch 0))))
        ("<home>"
         (lambda ()
           (interactive)
           (if (equal 2 exwm-workspace-current-index)
               (exwm-workspace-switch 3)
             (exwm-workspace-switch 2))))
        ("<f5>"
         (lambda () (interactive) (start-process-shell-command "" nil "slock")))
        ("<XF86AudioRaiseVolume>"
         (lambda ()
           (interactive)
           (call-process-shell-command
            "pactl set-sink-volume @DEFAULT_SINK@ +2000")))
        ("<XF86AudioLowerVolume>"
         (lambda ()
           (interactive)
           (call-process-shell-command
            "pactl set-sink-volume @DEFAULT_SINK@ -2000")))
        ("<XF86AudioMute>"
         (lambda ()
           (interactive)
           (call-process-shell-command
            "pactl set-sink-mute @DEFAULT_SINK@ toggle")))
        ("<XF86MonBrightnessUp>"
         (lambda () (interactive)
           (call-process-shell-command "brightnessctl set +10%")))
        ("<XF86MonBrightnessDown>"
         (lambda () (interactive)
           (call-process-shell-command "brightnessctl set 10%-")))))))
  (add-hook 'exwm-init-hook #'/exwm-setup-init)
  (defun /exwm-setup-manage-finish ()
    (pcase exwm-class-name
      ("mpv" (exwm-layout-set-fullscreen)))
    (pcase (buffer-name)
      ("Firefox" (exwm-workspace-move-window 1))
      ("Firefox<2>" (exwm-workspace-move-window 2))))
  (add-hook 'exwm-manage-finish-hook #'/exwm-setup-manage-finish)
  (setopt
   exwm-workspace-number 4
   exwm-input-prefix-keys (mapcar #'kbd '("C-x" "C-u" "M-x" "M-`" "M-&" "M-:"))
   exwm-input-simulation-keys
   (mapcar
    (lambda (keys) (cons (kbd (car keys)) (kbd (cadr keys))))
    '(("C-b" "<left>")
      ("C-S-b" "S-<left>")
      ("M-b" "C-<left>")
      ("M-B" "S-C-<left>")
      ("C-f" "<right>")
      ("C-S-f" "S-<right>")
      ("M-f" "C-<right>")
      ("M-F" "S-C-<right>")
      ("C-p" "<up>")
      ("C-S-p" "S-<up>")
      ("C-n" "<down>")
      ("C-S-n" "S-<down>")
      ("C-a" "<home>")
      ("C-S-a" "S-<home>")
      ("C-e" "<end>")
      ("C-S-e" "S-<end>")
      ("M-v" "<prior>")
      ("C-v" "<next>")
      ("C-k" "S-<end> C-x <delete>")
      ("C-w" "C-x")
      ("M-w" "C-c")
      ("C-y" "C-v")
      ("C-s" "C-f")
      ("C-=" "C-<left> S-C-<right>"))))
  (require 'exwm-randr)
  (defun /exwm-set-external-monitor (&optional arg)
    (interactive "P")
    (let ((regexp-connected "\n\\([^ ]+\\) connected")
          (regexp-disconnected "\n\\([^ ]+\\) disconnected"))
      (with-temp-buffer
        (call-process-shell-command "xrandr" nil t)
        (goto-char (point-min))
        (re-search-forward regexp-connected nil t)
        (let ((primary-output (match-string 1)))
          (if (re-search-forward regexp-connected nil t)
              (let ((auxiliary-output (match-string 1))
                    (position (if arg "--left-of" "--right-of")))
                (call-process-shell-command
                 (format
                  "xrandr --output %s --primary --auto --output %s --auto %s %s"
                  primary-output auxiliary-output position primary-output))
                (setopt
                 exwm-randr-workspace-monitor-plist
                 (list 0 auxiliary-output 1 auxiliary-output)))
            (progn
              (call-process-shell-command
               (format "xrandr --output %s --auto" primary-output))
              (goto-char (point-min))
              (while (re-search-forward regexp-disconnected nil t)
                (call-process-shell-command
                 (format "xrandr --output %s --off" (match-string 1))))))))))
  (defun /exwm-get-monitor-width-mm (mon) (nth 1 (assoc 'mm-size mon)))
  (defun /exwm-get-monitor-width-px (mon) (nth 3 (assoc 'geometry mon)))
  (defun /exwm-set-font-height ()
    (let* ((mon
            (seq-reduce
             (lambda (max-mon mon)
               (if (> (/exwm-get-monitor-width-mm mon)
                      (/exwm-get-monitor-width-mm max-mon))
                   mon
                 max-mon))
             (display-monitor-attributes-list)
             '((mm-size 0))))
           (height
            (pcase (list
                    (/exwm-get-monitor-width-mm mon)
                    (/exwm-get-monitor-width-px mon))
              ('(300 2880) 'tuxedo 210)
              ('(344 2560) 'msi 190)
              ('(508 1920) 'optoma 140)
              ('(597 2560) 'legion 130)
              ('(621 3840) 'jahnstr 190)
              ('(698 2560) 'station-f 130))))
      (when height (set-face-attribute 'default nil :height height))))
  (defun /exwm-setup-randr-screen-change (&optional arg)
    (interactive "P")
    (/exwm-set-external-monitor arg)
    (/exwm-set-font-height)
    (/setup-kinesis))
  (add-hook 'exwm-randr-screen-change-hook #'/exwm-setup-randr-screen-change)
  (exwm-randr-mode)
  (exwm-enable))
(keymap-global-set "<f6>" #'/exwm-setup-randr-screen-change)

(with-eval-after-load 'minibuffer
  (defun /export-to-dired ()
    (interactive)
    (let* ((candidates completion-all-sorted-completions)
           (length (safe-length candidates))
           (name (concat "Dired export: " (minibuffer-contents-no-properties))))
      (let ((pop-async (lambda () (dired (cons name (take length candidates)))))
            timer)
        (setq timer (run-with-timer nil nil pop-async)))
      (abort-minibuffers)))
  (defun /completing-read-in-region (start end collection &optional predicate)
    (let* ((initial (buffer-substring-no-properties start end))
           (all
            (completion-all-completions
             initial collection predicate (length initial)))
           (collection*
            (if (functionp collection)
                (lambda (str pred action) (funcall collection str pred action))
              collection))
           (completion
            (cond
             ((atom all) nil)
             ((and (consp all) (atom (cdr all)))
              (concat (substring initial 0 (cdr all)) (car all)))
             (t
              (completing-read
               "Complete: " collection* predicate nil initial)))))
      (cond
       (completion (completion--replace start end completion) t)
       (t (message "No completion") nil))))
  (keymap-unset minibuffer-local-completion-map "SPC")
  (keymap-unset minibuffer-local-completion-map "?")
  (keymap-set
   minibuffer-local-completion-map "C-. d" #'/complete-history-delete)
  (keymap-set
   minibuffer-local-completion-map "C-. e" #'/export-to-dired)
  (defun /minibuffer-setup () (setq-local truncate-lines t))
  (add-hook 'minibuffer-setup-hook #'/minibuffer-setup)
  (setopt
   completions-detailed t
   completion-ignored-extensions nil
   enable-recursive-minibuffers t
   completion-in-region-function #'/completing-read-in-region))

(with-eval-after-load 'icomplete
  (defun /icomplete-maybe-wrap-backward-completions ()
    (interactive)
    (if (equal 0 (length icomplete--scrolled-past))
        (icomplete-vertical-goto-last)
      (icomplete-backward-completions)))
  (defun /icomplete-maybe-wrap-forward-completions ()
    (interactive)
    (if (equal 1 (safe-length completion-all-sorted-completions))
        (icomplete-vertical-goto-first)
      (icomplete-forward-completions)))
  (defun /icomplete-scroll-completions (direction)
    (lambda ()
      (interactive)
      (dotimes (_ (1- (truncate (max-mini-window-lines) 1)))
        (if (equal direction 'forward)
            (icomplete-forward-completions)
          (icomplete-backward-completions)))))
  (keymap-unset icomplete-minibuffer-map "C-." t)
  (keymap-unset icomplete-fido-mode-map "C-." t)
  (keymap-set
   icomplete-minibuffer-map
   "<remap> <icomplete-backward-completions>"
   #'/icomplete-maybe-wrap-backward-completions)
  (keymap-set
   icomplete-minibuffer-map
   "<remap> <icomplete-forward-completions>"
   #'/icomplete-maybe-wrap-forward-completions)
  (keymap-set
   icomplete-minibuffer-map "<prior>" (/icomplete-scroll-completions 'backward))
  (keymap-set
   icomplete-minibuffer-map "<next>" (/icomplete-scroll-completions 'forward))
  (defun /icomplete-minibuffer-setup-setup ()
    (setq-local
     completion-styles (default-value 'completion-styles)
     completion-category-defaults
     (default-value 'completion-category-defaults)))
  (add-hook
   'icomplete-minibuffer-setup-hook #'/icomplete-minibuffer-setup-setup))

(with-eval-after-load 'isearch
  (setopt
   isearch-lazy-count t
   isearch-allow-scroll 'unlimited
   search-whitespace-regexp ".*?"))

(with-eval-after-load 'compile
  (setopt compilation-scroll-output t))

(with-eval-after-load 'proced
  (setopt proced-filter 'all proced-enable-color-flag t))

(with-eval-after-load 'comint
  (advice-add
   #'comint-add-to-input-history
   :filter-args
   #'/replace-spaces-before-save-advice)
  (setopt comint-prompt-read-only t))

(with-eval-after-load 'calendar
  (setopt calendar-week-start-day 1))

(with-eval-after-load 'recentf
  (setopt recentf-max-saved-items 200))

(with-eval-after-load 'help
  (setopt help-window-select t))

(with-eval-after-load 'pixel-scroll
  (keymap-unset pixel-scroll-precision-mode-map "<prior>")
  (keymap-unset pixel-scroll-precision-mode-map "<next>")
  (keymap-global-set
   "<remap> <scroll-up-command>" #'pixel-scroll-interpolate-down)
  (keymap-global-set
   "<remap> <scroll-down-command>" #'pixel-scroll-interpolate-up)
  (setopt pixel-scroll-precision-interpolate-page t))

(with-eval-after-load 'org
  (defun /org-setup-mode () (org-indent-mode))
  (add-hook 'org-mode-hook #'/org-setup-mode)
  (setopt
   org-fontify-done-headline t
   org-fontify-todo-headline t
   org-startup-truncated nil
   org-return-follows-link t))

(with-eval-after-load 'elfeed
  (defun /elfeed-yt-concat (collection id)
    (let ((collection* (symbol-name collection)))
      (concat
       "https://www.youtube.com/feeds/videos.xml?" collection* "_id=" id)))
  (defun /elfeed-show-entry-advice (orig-fun &rest args)
    (let ((url (elfeed-entry-link (car args))))
      (if (and
           (not current-prefix-arg)
           (string-match-p "https://www.youtube.com/watch" url))
          (/call-mpv url)
        (apply orig-fun args))))
  (advice-add #'elfeed-show-entry :around #'/elfeed-show-entry-advice)
  (defun /elfeed-filter-entry-by-title (feed-url junk-p)
    (add-hook
     'elfeed-new-entry-hook
     (elfeed-make-tagger
      :feed-url feed-url
      :entry-title junk-p
      :add 'junk
      :remove 'unread)))
  (/elfeed-filter-entry-by-title
   "UCCMxHHciWRBBouzk-PGzmtQ" (list 'not "Star Citizen"))
  (setopt
   elfeed-db-directory (expand-file-name "elfeed" user-emacs-directory)
   elfeed-search-filter "-junk "
   elfeed-feeds
   (list
    (list (/elfeed-yt-concat 'channel "UCqt99sKYNTxqlHtzV9weUYA") 'vu)
    (list (/elfeed-yt-concat 'channel "UC6107grRI4m0o2-emgoDnAA") 'smarter)
    (list (/elfeed-yt-concat 'channel "UCUHW94eEFW7hkUMVaZz4eDg") 'minute)
    (list (/elfeed-yt-concat 'channel "UC6nSFpj9HTCZ5t-N3Rm3-HA") 'vsauce)
    (list (/elfeed-yt-concat 'channel "UCCn27vl6uHBnDW5uYDoUpuQ") 'johatsu)
    (list (/elfeed-yt-concat 'channel "UC_yP2DpIgs5Y1uWC0T03Chw") 'grenier)
    (list (/elfeed-yt-concat 'channel "UCj1VqrHhDte54oLgPG4xpuQ") 'stuff)
    (list (/elfeed-yt-concat 'channel "UCCMxHHciWRBBouzk-PGzmtQ") 'bazar)
    (list (/elfeed-yt-concat 'channel "UCjsHDXUU3BjBCG7OaCbNDyQ") 'wonder)
    (list
     (/elfeed-yt-concat 'playlist "PLv1KZC6gJTFkFmZmlejLN6qyV7gvcmXwd")
     'rhinoceros)
    (list
     (/elfeed-yt-concat 'playlist "PLv1KZC6gJTFkzT_2k0dalYqvsM2GMfcz5")
     'epoque)
    (list
     (/elfeed-yt-concat 'playlist "PLEIYhPuy1thc0oRvCTQZlQZ91U7MpQDxS")
     'speedrun)
    '("https://fanfox.net/rss/one_piece.xml" piece)
    '("https://guix.gnu.org/feeds/blog.atom" guix)
    '("https://sachachua.com/blog/category/emacs-news/feed/index.xml" chua)
    '("http://parens-of-the-dead.com/atom.xml" undead))))
(keymap-global-set "<f8>" #'elfeed)

(with-eval-after-load 'dired
  (defun /dired-find-file-advice (orig-fun &rest args)
    (let* ((file (dired-get-file-for-visit))
           (ext (downcase (or (file-name-extension file) "")))
           (formatted-file
            (if-let ((youtube-id (cadr (string-split file "⊥"))))
                (concat "https://www.youtube.com/watch?v=" youtube-id)
              (prin1-to-string file))))
      (cond
       ((member ext '("jpg" "png" "jpeg")) (/call-feh formatted-file))
       ((or (member ext '("mkv" "mpg" "thm" "mp4" "avi" "ogv" "flv" "mov" "webm"
                          "wmv"))
            (string-match-p "⊥" file))
        (bookmark-set
         (file-name-nondirectory
          (directory-file-name (dired-current-directory))))
        (/call-mpv formatted-file))
       (t (apply orig-fun args)))))
  (advice-add #'dired-find-file :around #'/dired-find-file-advice)
  (advice-add #'dired-mouse-find-file :around #'/dired-find-file-advice)
  (keymap-set
   dired-mode-map
   "<remap> <dired-mouse-find-file-other-window>" #'dired-mouse-find-file)
  (defun /dired-setup-mode () (dired-hide-details-mode))
  (add-hook 'dired-mode-hook #'/dired-setup-mode)
  (setopt
   dired-listing-switches "-alv --group-directories-first"
   dired-create-destination-dirs 'ask
   dired-create-destination-dirs-on-trailing-dirsep t
   dired-dwim-target t)
  (when (require 'dired-subtree nil t)
    (keymap-set dired-mode-map "TAB" #'dired-subtree-cycle))
  (when (require 'dired-du nil t)
    (require 'ls-lisp) ;; TODO: https://github.com/calancha/dired-du/pull/5
    (defun /dired-hide-details-setup-mode ()
      (if dired-hide-details-mode
          (progn (dired-du-mode -1) (dired-sort-other dired-listing-switches))
        (progn
          (dired-du-mode)
          (dired-sort-other (concat dired-listing-switches " -S")))))
    (add-hook 'dired-hide-details-mode-hook #'/dired-hide-details-setup-mode)
    (setopt ls-lisp-use-insert-directory-program nil)))

(with-eval-after-load 'eshell
  (with-eval-after-load 'esh-module
    (unless (boundp 'eshell-modules-list) (load "esh-module"))
    (add-to-list 'eshell-modules-list 'eshell-elecslash))
  (with-eval-after-load 'esh-mode
    (unless (boundp 'eshell-output-filter-functions) (load "esh-mode"))
    (defun /eshell-complete-cd ()
      (interactive)
      (setq-local /complete-history-current eshell-last-dir-ring)
      (thread-last
        (/complete-history-function eshell-last-dir-ring)
        (completing-read "Eshell visited directories: ")
        (concat "cd ")
        insert)
      (eshell-send-input))
    (keymap-set eshell-mode-map "C-c d" #'/eshell-complete-cd)
    (add-to-list 'eshell-output-filter-functions #'eshell-truncate-buffer))
  (with-eval-after-load 'em-hist
    (keymap-unset eshell-hist-mode-map "M-s")
    (keymap-set
     eshell-hist-mode-map
     "<remap> <eshell-previous-matching-input>"
     (/complete-history "eshell" 'eshell-history-ring)))
  (defun /trim-path (path)
    (if (equal "/" path)
        path
      (car (last (split-string (abbreviate-file-name path) "/")))))
  (defun /eshell-prompt-function ()
    (let* ((path (/trim-path (eshell/pwd)))
           (tramp+path
            (if (and (fboundp #'tramp-tramp-file-p)
                     (tramp-tramp-file-p default-directory))
                (tramp-make-tramp-file-name
                 (tramp-dissect-file-name (eshell/pwd)) path)
              path))
           (success-or-failure (if (eshell-exit-success-p) " $ " " ! ")))
      (concat tramp+path success-or-failure)))
  (defvar /eshell-history-global-ring nil)
  (defun /eshell-hist-use-global-history ()
    (unless /eshell-history-global-ring
      (setopt /eshell-history-global-ring eshell-history-ring))
    (setq-local eshell-history-ring /eshell-history-global-ring))
  (defun /eshell-setup-mode ()
    (compilation-shell-minor-mode)
    (setq-local completion-ignore-case t)
    (/eshell-hist-use-global-history)
    (when (require 'with-editor nil t) (with-editor-export-editor)))
  (add-hook 'eshell-mode-hook #'/eshell-setup-mode)
  (setopt
   eshell-hist-ignoredups 'erase
   eshell-history-size 5000
   eshell-prompt-function #'/eshell-prompt-function

   ;; TODO: Remove after 30.1?
   eshell-prompt-regexp "^[^#$!\n]* [#$!] "

   eshell-destroy-buffer-when-process-dies t
   eshell-buffer-maximum-lines 10000))

;; TODO: Remove after 31.1?
(with-eval-after-load 'grep
  (require 'wgrep nil t))

(with-eval-after-load 'prog-mode
  (defun /prog-setup-mode ()
    (prettify-symbols-mode)
    (auto-fill-mode)
    (whitespace-mode)
    (setq-local fill-column 80)
    (display-fill-column-indicator-mode)
    (setq-local indent-tabs-mode nil)
    (when (require 'rainbow-delimiters nil t) (rainbow-delimiters-mode))
    (when (require 'smartparens nil t) (smartparens-strict-mode))
    (add-hook 'before-save-hook #'whitespace-cleanup nil t))
  (add-hook 'prog-mode-hook #'/prog-setup-mode))

(with-eval-after-load 'whitespace
  (setopt
   whitespace-line-column 80
   whitespace-style
   '(face tabs trailing lines space-before-tab indentation empty
          space-after-tab)))

(with-eval-after-load 'smartparens
  (require 'smartparens-config)
  (defun /sp-switch-pair ()
    (interactive)
    (pcase (plist-get (sp-get-enclosing-sexp) :op)
      ("(" (sp-rewrap-sexp '("[" . "]")))
      ("[" (sp-rewrap-sexp '("{" . "}")))
      ("{" (sp-rewrap-sexp '("(" . ")")))))
  (sp-use-paredit-bindings)
  (keymap-unset smartparens-mode-map "M-s")
  (keymap-unset smartparens-mode-map "M-?")
  (keymap-set smartparens-mode-map "C-@" #'/sp-switch-pair)
  (keymap-set smartparens-mode-map "M-m" #'sp-clone-sexp)
  (keymap-set smartparens-mode-map "C-o" #'sp-splice-sexp)
  (keymap-set smartparens-mode-map "C-?" #'sp-convolute-sexp))

(with-eval-after-load 'magit
  (setopt
   magit-log-margin '(t age-abbreviated magit-log-margin-width t 7)
   magit-log-section-commit-count 30
   git-commit-summary-max-length 72
   magit-diff-refine-hunk 'all))
(keymap-global-set "C-x g" #'magit-status)

(with-eval-after-load 'transient
  (setopt
   transient-values
   '((magit-log:magit-log-mode
      "-n256" "--graph" "--decorate" "--show-signature"))))

(setopt
 auto-mode-alist (add-to-list 'auto-mode-alist '("Makefile.*" . makefile-mode)))
(setenv "MAKEFLAGS" (concat "-j " /nb-proc))
(with-eval-after-load 'make-mode
  (defun /makefile-setup-mode () (setq-local indent-tabs-mode t))
  (add-hook 'makefile-mode-hook #'/makefile-setup-mode))

(/source-file "~/.opam/opam-init/init.sh")
(setenv "OPAMJOBS" /nb-proc)
(setenv "OPAMDOWNLOADJOBS" "5")
(let ((opam-share
       (with-temp-buffer
         (call-process-shell-command "printf %s $(opam var share)" nil t)
         (buffer-string))))
  (when (file-directory-p opam-share)
    (add-to-list 'load-path (expand-file-name "emacs/site-lisp" opam-share))))
(with-eval-after-load 'tuareg
  (setenv "PGHOST" "localhost")
  (setenv "PGDATABASE")
  (setenv "PGPORT" "5432")
  (setenv "PGUSER" "baptiste")
  (setenv "PGPASSWORD")
  (setenv "PGCOMMENT_SRC_LOC" "on")
  (setenv "PGCUSTOM_CONVERTERS_CONFIG")
  (add-hook 'tuareg-mode-hook #'eglot-ensure)
  (setopt tuareg-prettify-symbols-full t)
  (require 'utop)
  (keymap-set utop-mode-map "<remap> <complete-symbol>" #'utop-complete)
  (add-hook 'tuareg-mode-hook #'utop-minor-mode)
  (setopt utop-command "opam exec -- utop -emacs")
  (require 'ocamlformat)
  (defun /setup-ocamlformat-mode ()
    (add-hook 'before-save-hook #'ocamlformat-before-save nil t))
  (add-hook 'tuareg-mode-hook #'/setup-ocamlformat-mode))

(with-eval-after-load 'flymake
  (keymap-set flymake-mode-map "M-n" #'flymake-goto-next-error)
  (keymap-set flymake-mode-map "M-p" #'flymake-goto-prev-error))

(with-eval-after-load 'sql
  (defun /sql-save-local-history ()
    (let ((comint-input-ring-separator sql-input-ring-separator)
          (comint-input-ring-file-name sql-input-ring-file-name))
      (comint-write-input-ring)))
  (defun /sql-save-history ()
    (dolist (buffer (buffer-list))
      (with-current-buffer buffer
        (when (equal major-mode 'sql-interactive-mode)
          (/sql-save-local-history)))))
  (keymap-set
   sql-interactive-mode-map
   "<remap> <comint-history-isearch-backward-regexp>"
   (/complete-history "sql" 'comint-input-ring))
  (defun /setup-sql-interactive-mode ()
    (let ((sql-history-dir
           (expand-file-name "sql-history" user-emacs-directory)))
      (make-directory sql-history-dir t)
      (setq-local
       sql-input-ring-file-name
       (expand-file-name (symbol-name sql-connection) sql-history-dir))
      (when (require 'shell-switcher nil t)
        (shell-switcher-manually-register-shell))
      (add-hook 'kill-buffer-hook #'/sql-save-local-history nil t)))
  (add-hook 'sql-interactive-mode-hook #'/setup-sql-interactive-mode)
  (add-hook 'sql-mode-hook #'eglot-ensure)
  (add-hook 'kill-emacs-hook #'/sql-save-history)
  (setopt
   sql-connection-alist
   '(("local"
      (sql-port 5432)
      (sql-server "localhost")
      (sql-user "postgres")
      (sql-database "postgresql://postgres:@localhost/postgres")))
   sql-product 'postgres))
(defun /sql-define-connection (name &optional host)
  (fset
   (intern (concat "sql-" (symbol-name name)))
   (lambda ()
     (interactive)
     (let ((default-directory (or host default-directory)))
       (sql-connect name)))))
(/sql-define-connection 'local)

(with-eval-after-load 'geiser
  (keymap-unset geiser-repl-mode-map "<home>")
  (keymap-set
   geiser-repl-mode-map
   "<remap> <comint-history-isearch-backward-regexp>"
   (/complete-history "scheme" 'comint-input-ring))
  (defun /geiser-setup-mode ()
    (when (require 'shell-switcher nil t)
      (shell-switcher-manually-register-shell)))
  (add-hook 'geiser-repl-startup-hook #'/geiser-setup-mode))

(with-eval-after-load 'clojure-mode
  (defun /setup-clojure-mode ()
    (eglot-ensure)
    (add-hook 'before-save-hook #'eglot-format-buffer nil t))
  (add-hook 'clojure-mode-hook #'/setup-clojure-mode))

;; TODO: https://github.com/clojure-emacs/cider/pull/3574
(add-to-list 'load-path "~/Documents/programmes/lisp/elisp/cider")

(with-eval-after-load 'cider-mode
  (defun /cider-insert-reset-in-repl ()
    (interactive)
    (cider-insert-in-repl "(reset)" t))
  (keymap-set cider-mode-map "C-c C-j C-c" #'/cider-insert-reset-in-repl)
  (keymap-unset cider-repl-mode-map "M-s")
  (keymap-set
   cider-repl-mode-map
   "<remap> <cider-repl-previous-matching-input>"
   (/complete-history "cider" 'cider-repl-input-history))
  (defun /setup-cider-repl-mode ()
    (when (require 'shell-switcher nil t)
      (shell-switcher-manually-register-shell)))
  (add-hook 'cider-repl-mode-hook #'/setup-cider-repl-mode)
  (advice-add
   #'cider-repl--add-to-input-history
   :filter-args
   #'/replace-spaces-before-save-advice)
  (setopt
   cider-repl-history-file
   (expand-file-name "cider-history" user-emacs-directory)
   cider-repl-history-size 10000
   cider-clojure-cli-aliases ":dev:test"
   cider-offer-to-open-cljs-app-in-browser nil
   cider-shadow-watched-builds '("app")))

(with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs '(clojure-ts-mode . ("clojure-lsp")))
  (add-to-list 'eglot-server-programs '(sql-mode ("sqls")))
  (defun /setup-eglot-managed-mode ()
    (when (member major-mode '(clojure-mode clojurescript-mode clojurec-mode))
      (remove-hook
       'eldoc-documentation-functions #'eglot-signature-eldoc-function t)
      (remove-hook
       'eldoc-documentation-functions #'eglot-hover-eldoc-function t)
      (add-hook
       'eldoc-documentation-functions #'eglot--highlight-piggyback nil t))
    (when (equal major-mode 'sql-mode)
      (defun /eglot-switch-database ()
        (jsonrpc-request
         (eglot-current-server)
         :workspace/executeCommand
         `(:command "switchDatabase" :arguments ["postgres"] :timeout 0.5)))
      (run-with-timer nil nil #'/eglot-switch-database)))
  (add-hook 'eglot-managed-mode-hook #'/setup-eglot-managed-mode))

(setopt
 exec-path
 (append
  (split-string (getenv "PATH") ":")
  (list (string-remove-suffix "/" exec-directory))))
