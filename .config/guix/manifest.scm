;; This "manifest" file can be passed to 'guix package -m' to reproduce
;; the content of your profile.  This is "symbolic": it only specifies
;; package names.  To reproduce the exact same profile, you also need to
;; capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(specifications->manifest
  (list "emacs-gruvbox-theme"
        "stumpwm"
        "emacs-exwm"
        "transmission"
        "udisks"
        "pulseaudio"
        "emacs"
        "yt-dlp"
        "emacs-lemon"
        "firefox"
        "xournal"
        "feh"
        "mpv"
        "git"
        "emacs-magit"
        "pandoc"
        "xorg-server"
        "brightnessctl"
        "emacs-dired-hacks"
        "imagemagick"
        "curl"
        "emacs-elfeed"
        "setxkbmap"
        "cryptsetup"
        "adb"
        "xss-lock"
        "gnupg"
        "unison"
        "emacs-geiser-guile"
        "emacs-with-editor"
        "emacs-geiser"
        "xinit"
        "lz4json"
        "libjpeg"
        "python"
        "emacs-orderless"
        "sqlite"
        "xrandr"
        "slock"
        "xinput"
        "xmodmap"
        "emacs-shell-switcher"
        "jq"
        "strace"
        "autoconf"
        "make"
        "automake"
        "binutils"
        "emacs-gcmh"
        "emacs-expand-region"
        "emacs-dired-du"
        "emacs-wgrep"
        "emacs-smartparens"
        "emacs-rainbow-delimiters"
        "zip"
        "file"
        "unzip"
        "emacs-multiple-cursors"
        "font-dejavu"
        "glibc-locales"))
