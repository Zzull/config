(use-modules (gnu))

(define %user-name "baptiste")

(define nongnu?
  (with-exception-handler
      (lambda (_) #f)
    (lambda () (resolve-interface '(nongnu packages linux)))
    #:unwind? #t))

(define %modified-desktop-services
  (modify-services
   (@ (gnu services desktop) %desktop-services)
   (delete (@ (gnu services xorg) gdm-service-type))
   (mingetty-service-type
    config =>
    (if (equal? "tty1" (mingetty-configuration-tty config))
        (mingetty-configuration
         (inherit config)
         (auto-login %user-name)
         (login-pause? #t))
        config))
   ((@ (gnu services desktop) elogind-service-type)
    config =>
    ((@ (gnu services desktop) elogind-configuration)
     (inherit config)
     (handle-lid-switch-docked 'suspend)
     (handle-lid-switch-external-power 'suspend)))
   (guix-service-type
    config =>
    (guix-configuration
     (inherit config)
     (substitute-urls
      (cons* "https://substitutes.nonguix.org" %default-substitute-urls))
     (authorized-keys
      (cons*
       (plain-file
        "non-guix.pub"
        "(public-key (ecc (curve Ed25519) (q
#C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))")
       %default-authorized-guix-keys))))))

(operating-system
 (timezone "Europe/Paris")
 (keyboard-layout (keyboard-layout "fr" "bepo"))
 (host-name "guix")
 (users
  (cons* (user-account
          (name %user-name)
          (group "users")
          (supplementary-groups
           '("wheel" "netdev" "audio" "video" "input" "tty" "lp")))
         %base-user-accounts))
 (services
  (cons*
   (service (@ (gnu services ssh) openssh-service-type))
   (service (@ (gnu services networking) tor-service-type))
   (service (@ (gnu services cups) cups-service-type)
            ((@ (gnu services cups) cups-configuration)
             (web-interface? #t)
             (extensions
               (list
                (@ (gnu packages cups) cups-filters)
                (@ (gnu packages cups) brlaser)))))
   (service (@ (gnu services xorg) startx-command-service-type)
            ((@ (gnu services xorg) xorg-configuration)
             (keyboard-layout keyboard-layout)
             (extra-config (list "
Section \"Extensions\"
  Option \"DPMS\" \"Disable\"
EndSection"
                          "
Section \"ServerFlags\"
    Option \"StandbyTime\" \"0\"
    Option \"SuspendTime\" \"0\"
    Option \"OffTime\" \"0\"
    Option \"BlankTime\" \"0\"
EndSection"))))
   (extra-special-file
    (string-append "/home/" %user-name "/.bash_profile")
    (plain-file
     "bash-profile" "export GUIX_LOCPATH=$HOME/.guix-profile/lib/locale"))
   %modified-desktop-services))
 (kernel (if nongnu?
             (@ (nongnu packages linux) linux)
             (@ (gnu packages linux) linux-libre)))
 (initrd (if nongnu?
             (@ (nongnu system linux-initrd) microcode-initrd)
             base-initrd))
 (firmware (if nongnu?
               (list (@ (nongnu packages linux) linux-firmware))
               %base-firmware))
 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets (list "/boot/efi"))
   (extra-initrd "/@/key-file.cpio")))
 (mapped-devices
  (list (mapped-device
         (source
          (uuid "90b49da5-a317-4892-acf1-07c98f5bbc46"))
         (target "cryptpart")
         (type
          (luks-device-mapping-with-options #:key-file "/key-file.bin")))))
 (file-systems
  (cons* (file-system
          (mount-point "/boot/efi")
          (device (file-system-label "BOOTPART"))
          (type "vfat"))
         (file-system
          (mount-point "/")
          (device "/dev/mapper/cryptpart")
          (options "subvol=@")
          (type "btrfs")
          (dependencies mapped-devices))
         (file-system
          (mount-point "/home")
          (device "/dev/mapper/cryptpart")
          (options "subvol=@home")
          (type "btrfs")
          (dependencies mapped-devices))
         %base-file-systems)))
